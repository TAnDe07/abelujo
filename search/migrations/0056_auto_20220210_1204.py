# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0055_auto_20220204_1204'),
    ]

    operations = [
        migrations.AddField(
            model_name='distributor',
            name='client_number',
            field=models.CharField(max_length=200, null=True, verbose_name='Our client number with this supplier', blank=True),
        ),
    ]
