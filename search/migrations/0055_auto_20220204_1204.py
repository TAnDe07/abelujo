# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

from search.models import Client

"""
Data migration: populate the new Client.name_ascii and firstname_ascii fields.
"""

def populate_client_ascii(app, schema_editor):
    pass
    # Fails when we add a field in DB in a future migration file
    # that is not yet present in the DB.
    #
    # clients = Client.objects.all()
    # for client in clients:
    #     # should do it:
    #     client.save()


def noop(app, schema_editor):
    # nothing special to migrate backwards.
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0054_auto_20220204_1157'),
    ]

    operations = [
        migrations.RunPython(populate_client_ascii, noop),
    ]
