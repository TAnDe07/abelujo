# Copyright (c) Vincent Dardel, <vindarel@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.

# bookshops dependencies.
-e search/datasources/

pip==19
setuptools==39.2

# deps of deps, necessary for a successful install.
# Agressively pin dependencies with other tool?
regex==2020.11.13

django==1.8
django-bootstrap3==6.2.2
pyjade                  # process jade templates.
bootstrap-admin==0.3.6  # bootstrap theme, nice search feature.
pyyaml==5.4.1
pytz==2020.5

# Python2/3
six==1.15.0

### Django extensions
djangorestframework==3.4 # REST framework. Serve API, serialize models, explore schema, etc
django_extensions==1.7  # more useful commands to manage.py (shell_plus).
django-picklefield==1.1 # required to pin.
django-redis-cache==1.7.1
Werkzeug==0.15.2        # runserver_plus: richer tracebacks and inline console.

### utils
cachetools==2.1         # cache expensive function calls with TTL.
toolz==0.9.0            # pure, lazy, composable functional programming.
tabulate==0.8.3         # pretty print tabular data.
addict==2.2.0           # dotted notation to dict attributes and more.
unidecode==1.0.23       # ASCII transliteration of unicode text.
distance                # between two strings.
clize==3                # build command line arguments
tqdm==4.31              # progress bar
termcolor==1.1.0        # colorful terminal output
isbnlib<4               # isbn utils
unicodecsv<0.15         # at last unicode support to read and write csv files. Lacking in the standard library.
cairocffi==0.6          # for weasyprint. Later version fails.
WeasyPrint==0.34
viivakoodi              # formerly pybarcode
pillow==4.0             # needs libfreetype6-dev installed
dateparser==0.5         # parser for human readable dates

# deployment
whitenoise==2.0         # serve static files. Easily, efficiently.
gunicorn==19.4          # wsgi server

# temporary fixes ?
html5lib==1.0b8         # for "cannot import viewkeys" runtime error.

pendulum==1.1           # easy timezone, diffs, human info, etc
raven==6.0              # sentry

# To remove the yaml.load unsafe warning (of third party libs).
ruamel.yaml==0.16.12
ruamel.yaml.clib==0.2.2
ruamel.ordereddict==0.4.15

#
# Only required for experimental features.
# We install by default anyways, so the unit tests pass with no workarounds.
#
stripe==2.60.0

# Emails
sendgrid==6.7.0

# SMS
twilio==6.63.2  # last py 2.7 release.

#
# Deps of deps
#
certifi==2020.12.5
lazy-object-proxy==1.5.2

cffi==1.14.4
cryptography==3.3.1
distlib==0.3.1
docutils==0.9  # < 0.10 for our Sphinx version
Fabric==1.14.1
filelock==3.0.12
futures==3.3.0
jdatetime==3.6.2
paramiko==2.7.2
pathlib2==2.3.5
py==1.10.0
python-dateutil==2.8.1  # minor
python-http-client==3.3.2  # minor
starkbank-ecdsa==1.1.0
typing==3.7.4.3
virtualenv==20.4.0
wcwidth==0.2.5
wrapt==1.12.1
backports.functools-lru-cache==1.6.1
